-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 20-Set-2018 às 00:03
-- Versão do servidor: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `motojoy`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `brands`
--

CREATE TABLE `brands` (
  `brand_id` int(11) NOT NULL,
  `brand_name` varchar(255) NOT NULL,
  `brand_image` varchar(255) NOT NULL,
  `cat_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `brands`
--

INSERT INTO `brands` (`brand_id`, `brand_name`, `brand_image`, `cat_id`) VALUES
(2, 'K&N', '', 3),
(3, 'CNC', '', 6),
(4, 'Skyrich', '', 3),
(6, 'Rizoma', '', 6),
(7, 'Puig - Protecoes', '', 7),
(8, 'Shogun', '', 7),
(9, 'Puig - Acessorios', '', 6),
(15, 'Akrapovic', '', 3),
(16, 'BMC', '', 3),
(17, 'SC Project', '', 3),
(18, 'Dynojet', '', 3);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cart`
--

CREATE TABLE `cart` (
  `cart_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cart_prod_qty` int(11) NOT NULL,
  `cart_prod_total` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `categories`
--

CREATE TABLE `categories` (
  `cat_id` int(11) NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `cat_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `categories`
--

INSERT INTO `categories` (`cat_id`, `cat_name`, `cat_type`) VALUES
(3, 'Performance', ''),
(5, 'Geral', ''),
(6, 'Acessorios', ''),
(7, 'Protecoes', '');

-- --------------------------------------------------------

--
-- Estrutura da tabela `customer_order`
--

CREATE TABLE `customer_order` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `order_qty` varchar(10) NOT NULL,
  `order_amount` varchar(11) NOT NULL,
  `tran_id` varchar(300) NOT NULL,
  `order_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `groups`
--

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `group_permissions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `group_permissions`) VALUES
(1, 'Comum', ''),
(2, 'Administrador', '{\"admin\": 1}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `newspapper`
--

CREATE TABLE `newspapper` (
  `news_id` int(11) NOT NULL,
  `news_title` varchar(255) NOT NULL,
  `news_subtitle` varchar(255) NOT NULL,
  `news_description` text NOT NULL,
  `news_author` varchar(255) NOT NULL,
  `news_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `news_cat` varchar(255) NOT NULL,
  `news_image` varchar(255) NOT NULL,
  `news_views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `newspapper`
--

INSERT INTO `newspapper` (`news_id`, `news_title`, `news_subtitle`, `news_description`, `news_author`, `news_date`, `news_cat`, `news_image`, `news_views`) VALUES
(16, 'Honda celebra 100 milhÃµes de Super Cub', 'O veÃ­culo motorizado mais vendido do Mundo, a Super Cub, atingiu uma produÃ§Ã£o acumulada de 100 milhÃµes de unidades.', 'Foi hoje anunciado pela Honda Motor Co. que a produÃ§Ã£o acumulada da sua Super Cub, lanÃ§ada em 1958, atingiu a fasquia dos 100 milhÃµes de unidades produzidas, reforÃ§ando a reputaÃ§Ã£o daquele que se tornou o veÃ­culo motorizado mais vendido em todo o Mundo â€“ motos e automÃ³veis incluÃ­dos.<br /><br /><br />\r\nEste marco foi assinalado com uma cerimÃ³nia comemorativa levada a cabo na fÃ¡brica de Kumamoto, no JapÃ£o, com a presenÃ§a do Presidente e CEO da empresa, Takahiro Hachigo.<br /><br /><br />\r\nA primeira versÃ£o da Super Cub, a C100, com um motor de 50 cc a quatro tempos, refrigerado por ar, foi lanÃ§ada no verÃ£o de 1958, e hoje em dia Ã© produzida em 16 fÃ¡bricas espalhadas por 15 paÃ­ses em todo o Mundo.  O sucesso imediato nas vendas deste modelo (trÃªs anos depois atingiu-se o primeiro milhÃ£o de unidades produzidas) deveu-se a uma diversidade de caracterÃ­sticas: a durabilidade e eficiÃªncia do seu motor a 4 tempos (por oposiÃ§Ã£o aos motores a 2 tempos que representavam o grosso da produÃ§Ã£o na altura), o seu design exclusivo, uma embraiagem centrÃ­fuga que nÃ£o necessitava de manete de embraiagem e a grande proteÃ§Ã£o para as pernas.', 'Marco Gomes', '2018-04-02 17:41:42', 'up', '0860548aed7ba3e096529709427cb6a88f869d04696242b20be4c9972d43bf51.jpg', 11),
(17, 'MalÃ¡sia: TÃ­tulo atÃ© Ã  Ãºltima!', 'Andrea Dovizioso vence e leva discussÃ£o do tÃ­tulo para a Ãºltima corrida do ano em ValÃªncia.', 'A tarefa nÃ£o era Ã  partida impossÃ­vel, mas era bastante complicada, mesmo tendo em conta que Sepang Ã© um circuito \"amigo\" das Ducati. Andrea Dovizioso (Ducati Factory) sabia que apÃ³s o pÃ©ssimo resultado em Phillip Island tinha de atacar neste Grande PrÃ©mio da MalÃ¡sia para impedir que Marc Marquez (Repsol Honda) conseguisse festejar jÃ¡ hoje o tÃ­tulo de MotoGP, e foi isso mesmo que o italiano fez ao longo de todo o fim de semana.<br /><br />\r\n<br /><br />\r\nAs condiÃ§Ãµes do circuito malaio na corrida nÃ£o eram de todo propÃ­cias a que os pilotos andassem nos limites, pois a chuva que comeÃ§ou a fazer-se sentir no final da corrida das Moto2, intensificou, e com isso o asfalto do circuito tornou-se numa verdadeira armadilha para os pilotos que tentassem rodar perto do limite.<br /><br />\r\n<br /><br />\r\nApÃ³s a confusÃ£o habitual nos momentos iniciais em que Dani Pedrosa (Repsol Honda) arrancou da \"pole position\", foi o francÃªs Johann Zarco (Monster Tech3 Yamaha) a mostrar que o tÃ­tulo de melhor rookie do ano em MotoGP lhe assenta na perfeiÃ§Ã£o, e acabou por ser o francÃªs a ditar a lei atÃ© Ã  entrada das Ãºltimas 14 voltas.<br /><br />\r\n<br /><br />\r\nMais atrÃ¡s, mas nÃ£o muito, Jorge Lorenzo (Ducati Factory) ia mostrando que em Sepang poderia mesmo conseguir a sua primeira vitÃ³ria com a marca italiana, mesmo Ã  chuva, com a moto nÂº99 a rodar sem grandes problemas no segundo lugar, tendo atrÃ¡s de si o companheiro de equipa Andrea Dovizioso que conseguiu em luta direta passar por Marc Marquez, que sabia que nesse momento nÃ£o precisava de arriscar tudo pois seria campeÃ£o com essa conjugaÃ§Ã£o de resultados.<br /><br />\r\n<br /><br />\r\nA 12 voltas do fim Lorenzo ataca a lideranÃ§a e passa mesmo por Zarco, numa altura em que o asfalto estava jÃ¡ a apresentar alguns pontos em que estava seco e outros ainda bastante molhados, exigindo mÃ¡xima concentraÃ§Ã£o. Dovizioso nÃ£o queria perder tempo atrÃ¡s de Zarco e tambÃ©m acabou por ultrapassar o francÃªs nessa luta pela segunda posiÃ§Ã£o, com Marquez incapaz de fazer o mesmo e por isso a ter de se contentar com o quarto lugar. O campeÃ£o de MotoGP nÃ£o se mostrava confiante, mas sabia que com Dovizioso em segundo ainda iria festejar o tÃ­tulo jÃ¡ hoje.<br /><br />\r\n<br /><br />\r\nMarquez pressionou Zarco durante algumas voltas, mas este nÃ£o era dia para arriscar em demasia, e os pontos de quarto sÃ£o bastante preciosos, pelo que Marquez acabou por deixar Zarco fugir com o lugar mais baixo do pÃ³dio numa altura em que mais Ã  frente na pista as duas Ducati iam dominando as operaÃ§Ãµes. Pela primeira vez nos Ãºltimos cinco Grandes PrÃ©mios Marquez nÃ£o termina no pÃ³dio, mas ainda assim mantÃ©m a lideranÃ§a do campeonato.', 'Marco Gomes', '2018-04-02 17:44:54', 'gp', '35a6017ddd513c32f68ae652d5897d61f8633d7515d9617c4241f90b77e6ffe7.jpg', 10),
(18, 'Yamaha YZF-R6 2018', 'Foi uma decisÃ£o corajosa mas o resultado estÃ¡ de acordo com o desejado. A nova R6 Ã© excelente!', 'A Ãºltima geraÃ§Ã£o da R6, lanÃ§ada em 2008, coincidiu com o inÃ­cio da queda acentuada que o segmento das Supersport sofreu na Europa, valendo hoje esta classe cerca de 3.000 unidades, menos de um dÃ©cimo do mÃ¡ximo registado em 2006. As consequÃªncias sÃ£o conhecidas: as marcas deixaram de lanÃ§ar novos modelos, ficando-se na maioria dos casos por alteraÃ§Ãµes na decoraÃ§Ã£o, e pouco mais. Como se nÃ£o bastasse, a entrada em vigor da normativa Euro 4, obrigando Ã  acentuada reduÃ§Ã£o das emissÃµes poluentes, tornou ainda menos justificÃ¡vel comercialmente o investimento das marcas nesta classe.<br />\r\n<br />\r\nÃ‰ precisamente neste quadro recessivo que a Yamaha vislumbra uma oportunidade. Uma nova R6 pode ficar com todo o mercado e, atÃ©, aproveitar um eventual crescimento da categoria, jÃ¡ que a MV Agusta F3 675, a Ãºnica concorrente, pelas suas caraterÃ­sticas vive de argumentos comerciais distintos. Acresce ainda a vantagem de ter um modelo capaz de fazer a ponte entre a R3 e a R1, permitindo a fidelizaÃ§Ã£o dos clientes.<br />\r\n<br />\r\nTomada a decisÃ£o de continuar presente no segmento, impunha-se definir que R6 Ã© que devia ser proposta. O ponto de partida nÃ£o ajudou, jÃ¡ que o elevado rendimento do motor seria necessariamente afetado para se conseguir cumprir com a Euro 4, mas a performance absoluta nÃ£o se expressa apenas em potÃªncia, sendo igualmente importante a forma de a colocar no chÃ£o. A definiÃ§Ã£o estÃ©tica tambÃ©m tinha que ser repensada, pois a R6 anterior jÃ¡ contava com uma dÃ©cada de catÃ¡logo. Os estudos de opiniÃ£o efetuados mostraram que a evoluÃ§Ã£o passava pelo incremento das valÃªncias desportivas em todas as Ã¡reas, desafio que foi seguramente aceite com enorme prazer.<br />\r\n<br />\r\nEsteticamente o resultado superou as expetativas. Por vÃ¡rias vezes perguntei aos responsÃ¡veis pelas marcas porque razÃ£o nÃ£o aproximavam as versÃµes desportivas da imagem dos protÃ³tipos com que participavam, e havia sempre razÃµes objetivas que o impediam. Ã‰ assim com prazer que olho para esta R6 e identifico, claramente, a frente da M1, quase nÃ£o me apercebo dos farÃ³is e o meu olhar Ã© forÃ§ado pelas formas laterais da carenagem a fixar-se numa das mais belas traseiras do momento, claramente inspirada na da R1. Esta fluidez visual nÃ£o Ã© enganadora, a marca nÃ£o sÃ³ reclama uma melhoria de 8% na resistÃªncia ao vento como garante que esta Ã© a Yamaha mais aerodinÃ¢mica de sempre!<br />\r\n<br />\r\nRelativamente ao motor, as novidades podem ser vistas pela positiva, ou seja, apesar das restriÃ§Ãµes impostas pela Euro 4 os engenheiros conseguiram, atravÃ©s de um aumento do volume de ar na admissÃ£o, de alteraÃ§Ãµes no funcionamento mecÃ¢nico da injeÃ§Ã£o e dos mapas especÃ­ficos que criaram, compensar uma parte substancial do que os dois catalisadores presentes no sistema de escape retiraram. O valor de 118,4 cv Ã©, assim, digno de respeito!<br />\r\n<br />\r\nCiclisticamente as alteraÃ§Ãµes foram tambÃ©m profundas, mas todas no melhor sentido. NÃ£o havendo qualquer necessidade de tocar no excelente quadro e braÃ§o oscilante, alterou-se apenas o sub quadro, um pouco mais leve e estreito que anteriormente. O funcionamento do amortecedor traseiro foi revisto para oferecer um comportamento mais constante e melhorou-se a suspensÃ£o dianteira, agora ao nÃ­vel da R1, aumentando-se o diÃ¢metro de 41 para 43 mm, 5 mm mais no curso e completamente ajustÃ¡vel no topo da bainha. Foi tambÃ©m otimizada a rigidez da mesa de direÃ§Ã£o inferior e aumentado o diÃ¢metro do eixo dianteiro. Os travÃµes usufruÃ­ram tambÃ©m de uma evoluÃ§Ã£o, tendo a R1 como referÃªncia e montando na frente os mesmos discos de 320 mm e as pinÃ§as radiais de quatro pistÃµes da Nissin.<br />\r\n<br />\r\nO primeiro contacto Ã© bom, sente-se a leveza prÃ³pria de uma Supersport e o envolvimento proporcionado pela R6, sendo familiar, Ã© agora mais agradÃ¡vel. O painel de instrumentos tem uma distribuiÃ§Ã£o das informaÃ§Ãµes diferente, mais evidente, e como Ã© maior tudo se torna mais legÃ­vel. A operacionalidade do D-mode Ã© simples e permite escolher, atravÃ©s de um botÃ£o em cada um dos punhos, um dos trÃªs mapas para o motor e um dos seis nÃ­veis de controlo de traÃ§Ã£o. A Yamaha decidiu, para este teste em circuito, retirar os espelhos e montar o silenciador homologado para estrada da Akrapovic, que nÃ£o aumenta a performance mas oferece uma sonoridade mais interessante. Assim que comeÃ§amos a apertar sobressaem as diferenÃ§as. Em mÃ©dias a resposta Ã© interessante e, a partir das 8.000 rpm, sente-se que empurra o suficiente para sair das curvas em aceleraÃ§Ã£o. O valor mÃ¡ximo de binÃ¡rio, apesar de ter descido ligeiramente, Ã© atingido um pouco antes, agora Ã s 10.500 rpm, e nÃ£o se sente nenhuma inflexÃ£o ao longo da curva, atÃ© ao momento em que chegamos Ã s 13.000 rpm. Nessa altura, ou trocamos de caixa ou temos que esperar algumas dÃ©cimas de segundo, pelas 14.000 rpm, para usufruir de um Ãºltimo fÃ´lego atÃ© quase Ã s 16.000 rpm, nÃ£o valendo a pena levar o ponteiro atÃ© ao inÃ­cio da zona vermelha situada 500 rotaÃ§Ãµes acima. O som do escape Ã© bonito, a leitura do painel fÃ¡cil, o quickshifter dispensa cuidados e a proteÃ§Ã£o aerodinÃ¢mica proporcionada pelo ecrÃ£ mais alto Ã© excelente, permitindo-nos tudo isto concentrar na procura e manutenÃ§Ã£o da melhor trajetÃ³ria na difÃ­cil pista de Almeria.<br />\r\n<br />\r\nA Yamaha escolheu para equipamento original os Bridgestone S21, mas para este teste tinha montados os R10, verdadeiros pneus de competiÃ§Ã£o homologados para estrada. Surpreenderam-me pela aderÃªncia, jÃ¡ que, apesar de me esforÃ§ar, nunca senti a entrada em funcionamento do controlo de traÃ§Ã£o, e tambÃ©m pela longevidade, tendo ainda rasgos visÃ­veis apÃ³s quatro horas de utilizaÃ§Ã£o em pista. Se o bom funcionamento do motor nos dissipou as dÃºvidas relativamente Ã  capacidade para ultrapassar as limitaÃ§Ãµes impostas pela Euro 4, o funcionamento da ciclÃ­stica elevou a R6 a um novo patamar de eficÃ¡cia.<br />\r\n<br />\r\nSem perder a rapidez de direcÃ§Ã£o que a caraterizava, ganhou agora uma precisÃ£o na trajetÃ³ria que, combinada com a potÃªncia de travagem, a colocam como uma fonte de prazer se estivermos em pista. O novo depÃ³sito, mais estreito inicialmente, contribui para que nos movimentemos mais facilmente entre curvas e para uma posiÃ§Ã£o mais aconchegada quando vamos deitados.<br />\r\n<br />\r\nO acerto eletrÃ³nico mostrou-se tambÃ©m apurado em travagem, pois nunca senti o ABS a entrar em aÃ§Ã£o, nem mesmo na violenta travagem de sexta para segunda que antecede a entrada para as boxes.<br />\r\n<br />\r\nA Yamaha dispunha ainda de duas versÃµes com os kits que disponibiliza para os seus clientes, uma em versÃ£o WSS e outra, mais livre e com controlo de traÃ§Ã£o, para quem se quiser divertir em pista. Os onze quilos a menos, conjugados com o escape em titÃ¢nio, e os cavalos a mais oferecidos pelas peÃ§as que compÃµem o kit transformam em absoluto esta R6. Mesmo mantendo a mesma relaÃ§Ã£o de transmissÃ£o, respondia com mais vigor 2.000 rpm abaixo, abanava a frente Ã  saÃ­da das curvas e fazia mais 20 km/h de velocidade mÃ¡xima, tudo isto com a maior das facilidades, sem arrependimentos nem sustos, incentivando-nos a melhorar a cada volta efetuada.<br />\r\n<br />\r\nÃ‰ esta a magia de uma Supersport. Em vez de nos mostrar um limite e nos avisar das consequÃªncias que podem advir da tentativa de o ultrapassar, proporciona-nos sensaÃ§Ãµes positivas a cada volta que fazemos, retribuindo o nosso esforÃ§o com indicaÃ§Ãµes de como a melhorar. Experimentem!', 'Marco Gomes', '2018-04-02 17:57:54', 'lan', '3170983dab39093844338a078bf7792893b4c12c79d1cd514c8cd3f48ccc478b.jpg', 6),
(19, 'Moto2 MalÃ¡sia: Oliveira intocÃ¡vel!', 'Miguel Oliveira dominou por completo a corrida de Moto2 no dia em que Morbidelli se sagrou campeÃ£o.', 'Sob intenso calor - 34Âº de temperatura ambiente - decorreu hoje a penÃºltima corrida do ano do Mundial Moto2 no circuito de Sepang, a contar para o Grande PrÃ©mio da MalÃ¡sia. Franco Morbidelli (EG 0,0 Marc VDS) sabia Ã  partida que seria coroado campeÃ£o qualquer que fosse o resultado, pois em resultado do violento \"highside\" sofrido na qualificaÃ§Ã£o, Tom Luthi (CarXpert Interwetten) fraturou o tornozelo esquerdo e ficou fora de aÃ§Ã£o, \"oferecendo\" o tÃ­tulo ao italiano quando ainda falta realizar o Grande PrÃ©mio da Comunidade Valenciana.<br />\r\n<br />\r\nNo entanto quem mais brilhou hoje em Sepang voltou a ser o \"nosso\" Miguel Oliveira, que aos comandos da cada vez mais competitiva Red Bull KTM Ajo, voltou a repetir a dose da AustrÃ¡lia! Oliveira arrancou da segunda posiÃ§Ã£o da grelha para a corrida de 19 voltas, mas assim que os semÃ¡foros apagaram, o portuguÃªs saltou para a lideranÃ§a com um excelente arranque e a partir daÃ­ nunca mais foi incomodado.<br />\r\n<br />\r\nAo longo da corrida Miguel Oliveira foi sendo invariavelmente mais rÃ¡pido que o grupo de adversÃ¡rios que o perseguiu e que era composto por Franco Morbidelli, Brad Binder e Francesco Bagnaia, com o italiano da SKY VR46 a nÃ£o aguentar o ritmo de Morbidelli e Binder e a perder o contacto com o grupo que lutava pela segunda posiÃ§Ã£o.<br />\r\n<br />\r\nLÃ¡ na frente Miguel Oliveira foi registando volta rÃ¡pida atrÃ¡s de volta rÃ¡pida, estÃ¡ agora na sua posse o recorde do circuito em corrida, e no final da corrida, quando a cerca de cinco voltas do final comeÃ§aram a cair algumas gotas de Ã¡gua, o piloto da KTM acabou mesmo por gerir a vantagem atÃ© final, uma repetiÃ§Ã£o do que tinha acontecido hÃ¡ uma semana em Phillip Island, e terminou as 19 voltas com 2,3 segundos de vantagem sobre o seu companheiro de equipa Brad Binder, que tambÃ©m repetiu o excelente resultado da AustrÃ¡lia e garantiu nova dobradinha para a KTM, na frente de Franco Morbidelli que mesmo com o terceiro posto celebrou efusivamente a conquista do ambicionado tÃ­tulo das Moto2.<br />\r\n<br />\r\nCom a vitÃ³ria em Sepang, e com a desistÃªncia de Alex Marquez (EG 0,0 Marc VDS) ainda nas voltas iniciais apÃ³s duas quedas, Miguel Oliveira conseguiu tambÃ©m garantir desde jÃ¡ o terceiro lugar final no campeonato de Moto2, pois com 26 pontos de vantagem sobre o espanhol, e sÃ³ com a corrida de ValÃªncia para realizar, jÃ¡ nÃ£o hÃ¡ hipÃ³teses de Marquez remover Oliveira do merecido pÃ³dio do Mundial Moto2.<br />\r\n<br />\r\nNo final desta corrida perfeita para o jovem luso, a emoÃ§Ã£o era evidente na cara do piloto de Almada, que nÃ£o conteve as lÃ¡grimas enquanto se ouvia o hino nacional no pÃ³dio de Sepang e nÃ£o escondeu que teve de alterar a estratÃ©gia prevista para a corrida de hoje<br />\r\n<br />\r\n\"Sim, estou bastante cansado e nÃ£o sei se consigo dizer muitas palavras\", disse Miguel Oliveira em declaraÃ§Ãµes antes de subir ao pÃ³dio. \"A minha estratÃ©gia nÃ£o era ir embora, eu queria ficar atrÃ¡s do Morbidelli, seguir com ele. Mas no arranque e primeiras voltas percebi que tinha de ir embora. Ã‰ mais uma vitÃ³ria importante para nÃ³s\".<br />\r\n<br />\r\nJÃ¡ Franco Morbidelli, na anÃ¡lise ao momento em que conquista o tÃ­tulo de campeÃ£o, fez questÃ£o de destacar a rapidez das KTM e deixar uma palavra para o seu adversÃ¡rio que nÃ£o competiu por estar lesionado<br />\r\n<br />\r\n\"Tenho muita pena pelo Tom. Mas foi uma grande temporada para nÃ³s. Queria mesmo terminar no pÃ³dio para concluir a temporada da melhor forma e celebrar o tÃ­tulo, mas as KTM foram demasiado rÃ¡pidas hoje\".', 'Hugo Reis', '2018-04-02 18:18:15', 'gp', 'ae8699b7cded2aa090ae08e3b8a8124aa7edc43a86c48dd648a6b5cccf2110ad.jpg', 0),
(20, 'Kawasaki faz crescer a Ninja 300', 'Para os pilotos menos experientes a Kawasaki propÃµe uma Ninja 400 com melhores prestaÃ§Ãµes e \"look\" renovado.', 'A Kawasaki nÃ£o perdeu a oportunidade de este ano melhorar e muito as capacidades da sua pequena Ninja 300, que a partir de agora passa a ser Ninja 400.<br /><br />\r\n<br /><br />\r\nPara alÃ©m do design ter sofrido significativas alteraÃ§Ãµes e estando agora mais aproximado a modelos de outro porte da Kawasaki, o motor bicilÃ­ndrico sobe para os 399 cc, e com isso a potÃªncia cresce para uns generosos 45 cv Ã s 10.000 rpm, com a marca japonesa a garantir que apesar de manter um carÃ¡ter amigÃ¡vel para os mais inexperientes, consegue responder de forma mais imediata aos impulsos do acelerador.<br /><br />\r\n<br /><br />\r\nApesar de crescer em cilindrada, a Ninja 400 perdeu 6 kg em comparaÃ§Ã£o com a 300, sendo o seu peso a cheio de apenas 168 kg.<br /><br />\r\n<br /><br />\r\nOutros pontos em destaque, e de acordo com a Kawasaki, sÃ£o a melhor proteÃ§Ã£o aerodinÃ¢mica proveniente das carenagens redesenhadas, e tambÃ©m o facto do sistema de travagem estar agora equipado com os â€œmaiores discos de travÃ£oâ€ do segmento, com 310 mm de diÃ¢metro, sendo o ABS presenÃ§a obrigatÃ³ria.<br /><br />\r\n<br /><br />\r\nA Kawasaki Ninja 400 terÃ¡ um PVP desde 6.250â‚¬ mais ISV, e chegarÃ¡ aos concessionÃ¡rios em fevereiro de 2018.', 'Marco Gomes', '2018-04-02 18:33:17', 'up', 'a365268ac4b38cd7b163efca0d4bc848c552c9d81a78609cd178590a2c555202.jpg', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE `products` (
  `prod_id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `type_id` int(11) DEFAULT '5',
  `prom_id` int(11) DEFAULT NULL,
  `prod_name` varchar(255) NOT NULL,
  `prod_price` varchar(255) NOT NULL,
  `prod_old_price` int(11) NOT NULL,
  `prod_image` varchar(255) NOT NULL,
  `prod_desc` varchar(1024) NOT NULL,
  `prod_stock` int(11) NOT NULL DEFAULT '0',
  `prod_keywords` text NOT NULL,
  `prod_views` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`prod_id`, `cat_id`, `brand_id`, `type_id`, `prom_id`, `prod_name`, `prod_price`, `prod_old_price`, `prod_image`, `prod_desc`, `prod_stock`, `prod_keywords`, `prod_views`) VALUES
(29, 6, 3, 6, 0, 'Universal GP', '18.99', 20, '538cecd9875f0093af3fcefe42ff41a47c66f65f0870eadc7c93134902e3d6f2.png', 'Espelho universal de corrida', 0, 'espelho, corrida, universal, cnc', 108),
(30, 6, 6, 6, NULL, 'Espelhos bar end', '80.98', 85, 'd662d20c28d50e075a34610ee86471a16185a64fd9d8be4554f298ffcd4dc59f.jpg', 'Espelho cheio de estilo e discreto para as pontas do teu guiador.', 0, 'rizoma, espelho, bar, end', 13),
(31, 6, 3, 7, NULL, 'Punho universal 7/8\"', '7.98', 8, 'cf88fd4567052fa0e7ddba631689336b56dd0ed81076a1fa44bef8fe54820488.jpg', 'Par de punhos universais cheios de estilo', 4, 'punhos, cnc, universal, estilo', 60),
(32, 7, 8, 8, NULL, 'PA2', '142.90', 150, 'da0767ed57d3077d7fc6e2d701343721a978d6472bd060ddce0c658dde47e4d4.jpg', 'Cogumelos da shogun com eficÃ¡cia na proteÃ§Ã£o da sua mota durante uma queda.', 5, 'shogun, protecao, sliders, cogumelos, pa2', 28),
(33, 7, 7, 8, 0, 'Puig Pro ', '179.90', 190, '4346e15a3a833d4ffb61cd351311943646c877cbab171a2731a0c9cd292f5c90.jpg', 'Cogumelos de alta qualidade e eficÃ¡cia , sendo 98% resistente contra danos.', 2, 'puig, pro, cogumelos', 122),
(34, 3, 16, 12, 6, 'Race FM856', '59.90', 60, '0f9c268fa051e2b001bf0ac10ed2a47b219ce28262b5551a776b583424dd296d.jpg', 'O melhor na sua categoria', 6, 'bmc, filtro, race, fm, 856, fm856, performance', 47),
(35, 6, 3, 10, NULL, 'Kit manetes ajustÃ¡veis ', '18.90', 24, '0867cf1dce952a433bc5a6ad9860f4b2f40f1664f2b71978a8a755bfd89430b1.jpg', 'Kit de manetes ajustÃ¡veis e com um ar desportivo.', 2, 'manetes, cnc, kit, ajustaveis, desportivo', 18),
(36, 3, 15, 9, 7, 'GP', '233.98', 234, '08af57c155d7af52d906ad9c770a87f4564fc2299a7378b55454a5a1f9e37d62.jpg', 'Ponteira de alta qualidade com alto desempenho e com grande qualidade', 1, 'gp, akrapovic, escape, ponteira, desempenho, performance', 20),
(37, 3, 17, 9, 1, 'CR-T', '290.90', 296, '0da05a2179b246c91f571907b36fb316cb06400bec34b9e848cff408ea2949c6.jpg', 'Ponteira de alta qualidade e com uma entrega de performance soberba', 6, 'sc, project, ponteira, cr-t, performance, desempenho', 12),
(38, 3, 2, 12, NULL, 'RC-5166', '53.65', 59, 'c7c93acb0de6009e3e539adb2731113d772b79e701a76d0732beddf6acd69dae.jpg', 'Filtro de ar desportivo para obter desempenho mÃ¡ximo da sua mota.', 3, 'k&n, kn, filtro, desportivo, filtro de ar, desempenho, performance, rc-5166', 2),
(39, 3, 4, 11, NULL, 'Bateria de lÃ­tio ', '125.90', 131, '468997bcab204ef4b62618bce82d118d7025a4b89e080ffcf539d701f7e4edf3.jpg', 'Bateria ultra leve de lÃ­tio para alto desempenho', 3, 'litio, bateria, desempenho, performance, leve', 0),
(40, 3, 18, 13, NULL, 'Dynojet Power Commander V2', '89.90', 90, '0872352cce91dd2f0356f94b9d3f1ac0e776d500084376cd374cfeb8b5b6cf24.jpg', 'Centralina secundÃ¡ria para substituir a centralina principal.\r\n\r\n', 12, 'centralina, power,commander, v, dynojet', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `promotions`
--

CREATE TABLE `promotions` (
  `prom_id` int(11) NOT NULL,
  `prom_start` date NOT NULL,
  `prom_end` date NOT NULL,
  `prom_discount` varchar(4) NOT NULL,
  `prod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `promotions`
--

INSERT INTO `promotions` (`prom_id`, `prom_start`, `prom_end`, `prom_discount`, `prod_id`) VALUES
(1, '2018-09-19', '2018-10-15', '20', 37),
(6, '2018-05-13', '2018-10-11', '40', 34),
(7, '2018-05-13', '2018-10-30', '16', 36);

-- --------------------------------------------------------

--
-- Estrutura da tabela `received_payment`
--

CREATE TABLE `received_payment` (
  `pay_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `prod_id` int(11) NOT NULL,
  `pay_amount` varchar(11) NOT NULL,
  `pay_qty` int(11) NOT NULL,
  `tran_id` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `received_payment`
--

INSERT INTO `received_payment` (`pay_id`, `user_id`, `prod_id`, `pay_amount`, `pay_qty`, `tran_id`) VALUES
(3, 40, 33, '179.9', 1, '50H47418HD696973F'),
(4, 40, 29, '37.98', 2, '50H47418HD696973F'),
(6, 40, 36, '259.98', 1, '2G092441GB331761L'),
(7, 40, 35, '37.8', 2, '2G092441GB331761L'),
(8, 45, 32, '142.90', 1, '6WP41011WE854022A'),
(9, 40, 36, '259.98', 1, '5HY66631637374812'),
(10, 40, 33, '134.93', 1, '5E129819CX831653D');

-- --------------------------------------------------------

--
-- Estrutura da tabela `sessions`
--

CREATE TABLE `sessions` (
  `session_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `sessions`
--

INSERT INTO `sessions` (`session_id`, `user_id`, `hash`) VALUES
(1, 40, '9f535f822d8eb00590a13c4bf271d52fbe78443b9e368fd3418ebfed30bf8103');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tips_tricks`
--

CREATE TABLE `tips_tricks` (
  `tt_id` int(11) NOT NULL,
  `tt_title` varchar(255) NOT NULL,
  `tt_subtitle` varchar(255) NOT NULL,
  `tt_description` varchar(2000) NOT NULL,
  `tt_video` varchar(255) NOT NULL,
  `tt_author` varchar(255) NOT NULL,
  `tt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tt_image` varchar(255) NOT NULL,
  `tt_cat` varchar(255) NOT NULL,
  `tt_subcat` varchar(255) NOT NULL,
  `tt_views` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tips_tricks`
--

INSERT INTO `tips_tricks` (`tt_id`, `tt_title`, `tt_subtitle`, `tt_description`, `tt_video`, `tt_author`, `tt_date`, `tt_image`, `tt_cat`, `tt_subcat`, `tt_views`) VALUES
(6, 'Cabo vs. Embreagem HidrÃ¡ulica', 'Comparando os prÃ³s e contras de embreagens acionadas por cabo a embreagens hidrÃ¡ulicas', 'Quando falamos em embreagens hidrÃ¡ulicas ou de cabos, estamos apenas nos referindo a como a embreagem Ã© acionada. Com uma embreagem de cabo, a forÃ§a da alavanca Ã© transferida atravÃ©s de um cabo Bowden, que possui um fio de aÃ§o dentro de uma bainha flexÃ­vel. Com uma embreagem hidrÃ¡ulica, o fluido Ã© usado para transmitir forÃ§a como nos freios hidrÃ¡ulicos, exceto que, em vez de um compasso na outra extremidade da mangueira, hÃ¡ um cilindro escravo que age na placa de pressÃ£o da embreagem da mesma maneira que um cabo.<br /><br /><br /><br /><br />\r\nSe uma embreagem de cabo e uma embreagem hidrÃ¡ulica fazem a mesma coisa, por que um fabricante ou um piloto escolheria um sobre o outro? Cada sistema tem seus prÃ³s e contras, e comeÃ§aremos com a embreagem a cabo, jÃ¡ que Ã© a configuraÃ§Ã£o mais comum.<br /><br /><br /><br /><br />\r\nOs cabos sÃ£o comuns por um motivo simples: eles sÃ£o baratos de serem fabricados e fÃ¡ceis de instalar. Isso Ã© uma grande vantagem, quer vocÃª esteja fabricando motocicletas ou apenas mantendo sua prÃ³pria bicicleta. No tÃ³pico de manutenÃ§Ã£o, os cabos precisam de uma boa quantidade dele. Para comeÃ§ar, vocÃª precisa ajustar a folga com freqÃ¼Ãªncia para compensar o desgaste da embreagem - deixe o cabo solto demais e vocÃª nÃ£o conseguirÃ¡ se soltar completamente quando puxar a alavanca. Demasiado folga e vocÃª vai pegar embreagem e cozinhar seus pratos de embreagem. AlÃ©m disso, os cabos precisam ser lubrificados periodicamente, e eles sÃ£o suscetÃ­veis Ã  corrosÃ£o e quebra, alÃ©m de serem vinculados se forem dobrados com muita forÃ§a.<br /><br /><br /><br /><br />\r\nPor outro lado, vocÃª nÃ£o precisa se preocupar com lubrificaÃ§Ã£o ou cabos desgastados com uma embreagem hidrÃ¡ulica, e tambÃ©m nÃ£o precisa ajustar nada. Isso ocorre porque, enquanto houver fluido no reservatÃ³rio, um sistema hidrÃ¡ulico se ajustarÃ¡ automaticamente quando as chapas da embreagem se desgastarem, de modo que o ponto de engate permanece o mesmo duran', '<iframe width=\'560\' class=\'embed-responsive embed-responsive-16by9\' height=\'315\' src=\'https://www.youtube.com/embed/eJRDUvZH5Iw\' frameborder=\'0\' allow=\'autoplay; encrypted-media\' allowfullscreen></iframe>', 'Marco Gomes', '2018-04-20 13:51:52', 'f6d5f4e98cd0592765bd05898b900e6a4abedd68dcf3cfe859b981bd4286779b.jpg', 'manu', 'transmissao', 18),
(7, 'Erros mais comuns', 'A sua mota estÃ¡ estranha a conduzir? NÃ³s podemos ter a soluÃ§Ã£o', 'Praticamente qualquer problema que surja com a forma como a sua moto gira ou trava ou alÃ§as serÃ¡ o resultado de algo quebrando, desgastando ou vagando fora da especificaÃ§Ã£o. Se algo quebrar, a mudanÃ§a serÃ¡ repentina, enquanto outros problemas podem ser progressivos e piorar com o tempo.<br /><br />\r\n<br /><br />\r\nDe longe, o problema de manuseio mais comum que as pessoas enfrentam Ã© a direÃ§Ã£o pesada e de alto esforÃ§o. Se vocÃª acha que tem que usar muito mÃºsculo para iniciar um turno e entÃ£o vocÃª tem que manter a pressÃ£o nas manoplas para manter sua linha atravÃ©s de um canto, a primeira coisa que vocÃª deve verificar sÃ£o seus pneus. Um perfil desgastado causarÃ¡ esse tipo de manuseio, mas tambÃ©m os pneus com pouco enchimento, mesmo que sejam apenas dois ou trÃªs quilos.<br /><br />\r\n<br /><br />\r\nOutra reclamaÃ§Ã£o que ouvimos do pÃºblico do MC Garage Ã© sobre uma bicicleta que nÃ£o quer seguir em frente. Isso pode significar que ele desce a estrada ou atÃ© mesmo puxa para um lado. Se esse for o caso, a primeira coisa que vocÃª deseja verificar Ã© se as rodas estÃ£o alinhadas e se vocÃª falou com as rodas, verifique se nÃ£o hÃ¡ peÃ§as soltas. CabeÃ§a de direÃ§Ã£o frouxa, braÃ§o oscilante ou rolamentos de roda tambÃ©m podem ser os culpados. Mas nÃ£o confunda um problema real de instabilidade com \"tramlining\", que Ã© quando os pneus da sua moto seguem os sulcos na estrada. NÃ³s construÃ­mos concreto nas estradas aqui no SoCal, e isso faz com que pareÃ§a que hÃ¡ um fantasma dirigindo sua moto. NÃ£o hÃ¡ nada que vocÃª possa fazer sobre isso, a nÃ£o ser relaxar e deixar acontecer ou seguir um caminho diferente.', '<iframe width=\'560\' class=\'embed-responsive embed-responsive-16by9\' height=\'315\' src=\'https://www.youtube.com/embed/suizUBn8C-E\' frameborder=\'0\' allow=\'autoplay; encrypted-media\' allowfullscreen></iframe>', 'Hugo Reis', '2018-04-21 22:52:37', 'bcf79dccfa1b2e077f229a95cdc229cae4d0bdc5a7b40c625c795ca4df798bf1.jpg', 'cond', 'null', 5),
(8, 'Ã‰ mau andar sem ponteira de escape?', 'Quais sÃ£o os riscos se conduzir sem ponteira de escape?', 'Os atuais sistemas de exaustÃ£o de estoque sÃ£o silenciosos demais para a maioria das preferÃªncias dos ciclistas, e a palavra na rua (ou fÃ³runs, para ser mais especÃ­fico) Ã© que vocÃª nÃ£o precisa gastar US $ 500 em um slip-on para deixar sua moto mais alta. Isso Ã© porque sua bicicleta vai soar mal se arrancar apenas o silenciador de aÃ§Ãµes e executar um tubo aberto. Mas estÃ¡ andando sem um silenciador ruim para o motor, ou Ã© ruim para a humanidade? NÃ³s vamos ao fundo das coisas neste vÃ­deo da Garagem do MC.', '<iframe width=\'560\' class=\'embed-responsive embed-responsive-16by9\' height=\'315\' src=\'https://www.youtube.com/embed/KdWyDOvLzuE\' frameborder=\'0\' allow=\'autoplay; encrypted-media\' allowfullscreen></iframe>', 'Marco Gomes', '2018-04-21 23:24:49', '391990b053496d3aade4270bff08fe5cd02490ab81c1f5139883bae515edcddb.jpg', 'manu', 'motor', 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `typep`
--

CREATE TABLE `typep` (
  `type_id` int(11) NOT NULL,
  `cat_id` int(11) DEFAULT NULL,
  `type_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `typep`
--

INSERT INTO `typep` (`type_id`, `cat_id`, `type_name`) VALUES
(4, 5, 'geral'),
(6, 6, 'espelhos'),
(7, 6, 'punhos'),
(8, 7, 'Cogumelos'),
(9, 3, 'Ponteiras'),
(10, 6, 'Manetes'),
(11, 3, 'Bateria'),
(12, 3, 'Filtro de Ar'),
(13, 3, 'Centralina');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_username` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `user_salt` varchar(255) NOT NULL,
  `user_password_recover` int(11) NOT NULL,
  `user_password_flag` int(11) NOT NULL,
  `user_password_code` varchar(255) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_joined` datetime NOT NULL,
  `user_image` varchar(255) NOT NULL DEFAULT 'default.png',
  `user_nif` int(9) NOT NULL,
  `user_phone` int(9) NOT NULL,
  `user_address` varchar(255) NOT NULL,
  `user_postcode` varchar(255) NOT NULL,
  `user_confirm` varchar(255) DEFAULT NULL,
  `user_confirmed` int(11) NOT NULL DEFAULT '0',
  `user_news` int(11) NOT NULL DEFAULT '1',
  `group_id` int(11) NOT NULL,
  `cart_prods` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`user_id`, `user_email`, `user_username`, `user_password`, `user_salt`, `user_password_recover`, `user_password_flag`, `user_password_code`, `user_name`, `user_joined`, `user_image`, `user_nif`, `user_phone`, `user_address`, `user_postcode`, `user_confirm`, `user_confirmed`, `user_news`, `group_id`, `cart_prods`) VALUES
(40, 'admin@gmail.com', 'marcoAlexandre', 'a8a4adec020c768bec8f692f6a261e8c157a80eca8f1b4c3732a455573218e20', 'mÃ¢Ã‡Ã¼Â­4yÂ£Â’lÃ±Ã¬Â¹X1ÃÃLaeÃ›Ã–j\"Â…Ã–-ÃH', 0, 7, '', 'Marco511', '2017-12-04 18:58:49', 'banner2.jpg', 123456789, 912765629, 'Rua do Andrade 611', '4450-270', 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855', 1, 1, 2, 0),
(45, 'maria@hotmail.com', 'china', '6174f7002f8d51869c27f52349c7a452774e32d1cb920291e3fc254516ad5782', '\n]IÃ‚Ã¦Â¯ÃµÃ©CÂ»;!/Ã›Ã—ÂªÃ°Â¥Â~@Ã¿Â½Ã“WÂ„Ã¶Â˜', 0, 0, '', 'Marco', '2017-12-28 19:06:28', 'default.png', 912766620, 912731625, 'Rua das Marias 611', '4450-270', 'a0d8c083af49614abff4475031a8f5f734563330d63d48728ff1c1f7e02552cb', 1, 1, 1, 0),
(52, 'town@gmail.com', '', '4d7ee1f0e7defae5977f4940c1596deff817e8127e1e6f03fa73022a5abcfe04', 'Â¿Â¹ÃJ~Â˜Ã¹ÂœÃ¬HÂ£+Â³ÃƒgÂŽ(Â€V*Â¡Â”Â”IÂ¨9zn', 0, 0, '', 'Marco Alexandre', '2018-04-16 14:39:39', 'default.png', 0, 0, '', '', '5a892b6b04b70abbbd4cc5e7f041cafdd4a969721a9dc9bb6ec2198bdc90420e', 1, 1, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`brand_id`),
  ADD KEY `brands_cat_id` (`cat_id`);

--
-- Indexes for table `cart`
--
ALTER TABLE `cart`
  ADD PRIMARY KEY (`cart_id`),
  ADD KEY `cart_prod_id` (`prod_id`),
  ADD KEY `cart_user_id` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `customer_order`
--
ALTER TABLE `customer_order`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `order_user_id` (`user_id`),
  ADD KEY `order_prod_id` (`prod_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `newspapper`
--
ALTER TABLE `newspapper`
  ADD PRIMARY KEY (`news_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prod_id`),
  ADD KEY `products_brand_id` (`brand_id`),
  ADD KEY `products_cat_id` (`cat_id`),
  ADD KEY `products_type_id` (`type_id`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`prom_id`),
  ADD KEY `prom_prod_id` (`prod_id`);

--
-- Indexes for table `received_payment`
--
ALTER TABLE `received_payment`
  ADD PRIMARY KEY (`pay_id`),
  ADD KEY `pay_prod_id` (`prod_id`),
  ADD KEY `pay_user_id` (`user_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `tips_tricks`
--
ALTER TABLE `tips_tricks`
  ADD PRIMARY KEY (`tt_id`);

--
-- Indexes for table `typep`
--
ALTER TABLE `typep`
  ADD PRIMARY KEY (`type_id`),
  ADD KEY `type_cat_id` (`cat_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `brand_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `cart`
--
ALTER TABLE `cart`
  MODIFY `cart_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cat_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `customer_order`
--
ALTER TABLE `customer_order`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `newspapper`
--
ALTER TABLE `newspapper`
  MODIFY `news_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prod_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `prom_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `received_payment`
--
ALTER TABLE `received_payment`
  MODIFY `pay_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `session_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tips_tricks`
--
ALTER TABLE `tips_tricks`
  MODIFY `tt_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `typep`
--
ALTER TABLE `typep`
  MODIFY `type_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`cat_id`);

--
-- Limitadores para a tabela `cart`
--
ALTER TABLE `cart`
  ADD CONSTRAINT `cart_prod_id` FOREIGN KEY (`prod_id`) REFERENCES `products` (`prod_id`),
  ADD CONSTRAINT `cart_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Limitadores para a tabela `customer_order`
--
ALTER TABLE `customer_order`
  ADD CONSTRAINT `order_prod_id` FOREIGN KEY (`prod_id`) REFERENCES `products` (`prod_id`),
  ADD CONSTRAINT `order_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Limitadores para a tabela `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_brand_id` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`brand_id`),
  ADD CONSTRAINT `products_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`cat_id`),
  ADD CONSTRAINT `products_type_id` FOREIGN KEY (`type_id`) REFERENCES `typep` (`type_id`);

--
-- Limitadores para a tabela `promotions`
--
ALTER TABLE `promotions`
  ADD CONSTRAINT `prom_prod_id` FOREIGN KEY (`prod_id`) REFERENCES `products` (`prod_id`);

--
-- Limitadores para a tabela `received_payment`
--
ALTER TABLE `received_payment`
  ADD CONSTRAINT `pay_prod_id` FOREIGN KEY (`prod_id`) REFERENCES `products` (`prod_id`);

--
-- Limitadores para a tabela `typep`
--
ALTER TABLE `typep`
  ADD CONSTRAINT `type_cat_id` FOREIGN KEY (`cat_id`) REFERENCES `categories` (`cat_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
